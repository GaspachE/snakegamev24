// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowSpeed.h"
#include "SnakeBase.h"



// Sets default values
ASlowSpeed::ASlowSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlowSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlowSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASlowSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		FTimerHandle Handle;
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(0.7f);
		}		
	}
}

			