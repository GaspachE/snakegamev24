// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAMEV24_SnakeGameV24GameModeBase_generated_h
#error "SnakeGameV24GameModeBase.generated.h already included, missing '#pragma once' in SnakeGameV24GameModeBase.h"
#endif
#define SNAKEGAMEV24_SnakeGameV24GameModeBase_generated_h

#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_SPARSE_DATA
#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_RPC_WRAPPERS
#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGameV24GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGameV24GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGameV24GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGameV24"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGameV24GameModeBase)


#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGameV24GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGameV24GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGameV24GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGameV24"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGameV24GameModeBase)


#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGameV24GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGameV24GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGameV24GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGameV24GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGameV24GameModeBase(ASnakeGameV24GameModeBase&&); \
	NO_API ASnakeGameV24GameModeBase(const ASnakeGameV24GameModeBase&); \
public:


#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGameV24GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGameV24GameModeBase(ASnakeGameV24GameModeBase&&); \
	NO_API ASnakeGameV24GameModeBase(const ASnakeGameV24GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGameV24GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGameV24GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGameV24GameModeBase)


#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_12_PROLOG
#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_SPARSE_DATA \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_RPC_WRAPPERS \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_INCLASS \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_SPARSE_DATA \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAMEV24_API UClass* StaticClass<class ASnakeGameV24GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGameV24_Source_SnakeGameV24_SnakeGameV24GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
