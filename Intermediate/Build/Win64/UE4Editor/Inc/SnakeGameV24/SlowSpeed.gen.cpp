// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGameV24/SlowSpeed.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSlowSpeed() {}
// Cross Module References
	SNAKEGAMEV24_API UClass* Z_Construct_UClass_ASlowSpeed_NoRegister();
	SNAKEGAMEV24_API UClass* Z_Construct_UClass_ASlowSpeed();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGameV24();
	SNAKEGAMEV24_API UClass* Z_Construct_UClass_Uinteractable_NoRegister();
// End Cross Module References
	void ASlowSpeed::StaticRegisterNativesASlowSpeed()
	{
	}
	UClass* Z_Construct_UClass_ASlowSpeed_NoRegister()
	{
		return ASlowSpeed::StaticClass();
	}
	struct Z_Construct_UClass_ASlowSpeed_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASlowSpeed_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGameV24,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowSpeed_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SlowSpeed.h" },
		{ "ModuleRelativePath", "SlowSpeed.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASlowSpeed_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_Uinteractable_NoRegister, (int32)VTABLE_OFFSET(ASlowSpeed, Iinteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASlowSpeed_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASlowSpeed>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASlowSpeed_Statics::ClassParams = {
		&ASlowSpeed::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASlowSpeed_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASlowSpeed_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASlowSpeed()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASlowSpeed_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASlowSpeed, 4023710483);
	template<> SNAKEGAMEV24_API UClass* StaticClass<ASlowSpeed>()
	{
		return ASlowSpeed::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASlowSpeed(Z_Construct_UClass_ASlowSpeed, &ASlowSpeed::StaticClass, TEXT("/Script/SnakeGameV24"), TEXT("ASlowSpeed"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASlowSpeed);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
