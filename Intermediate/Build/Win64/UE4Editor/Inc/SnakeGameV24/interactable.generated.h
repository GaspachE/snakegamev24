// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAMEV24_interactable_generated_h
#error "interactable.generated.h already included, missing '#pragma once' in interactable.h"
#endif
#define SNAKEGAMEV24_interactable_generated_h

#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_SPARSE_DATA
#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_RPC_WRAPPERS
#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKEGAMEV24_API Uinteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Uinteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKEGAMEV24_API, Uinteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Uinteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKEGAMEV24_API Uinteractable(Uinteractable&&); \
	SNAKEGAMEV24_API Uinteractable(const Uinteractable&); \
public:


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKEGAMEV24_API Uinteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKEGAMEV24_API Uinteractable(Uinteractable&&); \
	SNAKEGAMEV24_API Uinteractable(const Uinteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKEGAMEV24_API, Uinteractable); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Uinteractable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Uinteractable)


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUinteractable(); \
	friend struct Z_Construct_UClass_Uinteractable_Statics; \
public: \
	DECLARE_CLASS(Uinteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/SnakeGameV24"), SNAKEGAMEV24_API) \
	DECLARE_SERIALIZER(Uinteractable)


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_GENERATED_UINTERFACE_BODY() \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_GENERATED_UINTERFACE_BODY() \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~Iinteractable() {} \
public: \
	typedef Uinteractable UClassType; \
	typedef Iinteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~Iinteractable() {} \
public: \
	typedef Uinteractable UClassType; \
	typedef Iinteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_10_PROLOG
#define SnakeGameV24_Source_SnakeGameV24_interactable_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_SPARSE_DATA \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_RPC_WRAPPERS \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGameV24_Source_SnakeGameV24_interactable_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_SPARSE_DATA \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGameV24_Source_SnakeGameV24_interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAMEV24_API UClass* StaticClass<class Uinteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGameV24_Source_SnakeGameV24_interactable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
