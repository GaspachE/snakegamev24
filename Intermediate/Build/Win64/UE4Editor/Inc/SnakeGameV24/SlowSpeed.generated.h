// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAMEV24_SlowSpeed_generated_h
#error "SlowSpeed.generated.h already included, missing '#pragma once' in SlowSpeed.h"
#endif
#define SNAKEGAMEV24_SlowSpeed_generated_h

#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_SPARSE_DATA
#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_RPC_WRAPPERS
#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASlowSpeed(); \
	friend struct Z_Construct_UClass_ASlowSpeed_Statics; \
public: \
	DECLARE_CLASS(ASlowSpeed, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGameV24"), NO_API) \
	DECLARE_SERIALIZER(ASlowSpeed) \
	virtual UObject* _getUObject() const override { return const_cast<ASlowSpeed*>(this); }


#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASlowSpeed(); \
	friend struct Z_Construct_UClass_ASlowSpeed_Statics; \
public: \
	DECLARE_CLASS(ASlowSpeed, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGameV24"), NO_API) \
	DECLARE_SERIALIZER(ASlowSpeed) \
	virtual UObject* _getUObject() const override { return const_cast<ASlowSpeed*>(this); }


#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASlowSpeed(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASlowSpeed) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASlowSpeed); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASlowSpeed); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASlowSpeed(ASlowSpeed&&); \
	NO_API ASlowSpeed(const ASlowSpeed&); \
public:


#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASlowSpeed(ASlowSpeed&&); \
	NO_API ASlowSpeed(const ASlowSpeed&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASlowSpeed); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASlowSpeed); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASlowSpeed)


#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_PRIVATE_PROPERTY_OFFSET
#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_10_PROLOG
#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_SPARSE_DATA \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_RPC_WRAPPERS \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_INCLASS \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_PRIVATE_PROPERTY_OFFSET \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_SPARSE_DATA \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_INCLASS_NO_PURE_DECLS \
	SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAMEV24_API UClass* StaticClass<class ASlowSpeed>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGameV24_Source_SnakeGameV24_SlowSpeed_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
