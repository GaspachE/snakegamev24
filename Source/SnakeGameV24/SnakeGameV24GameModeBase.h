// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameV24GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMEV24_API ASnakeGameV24GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
