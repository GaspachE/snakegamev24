// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SnakeGameV24.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGameV24, "SnakeGameV24" );
