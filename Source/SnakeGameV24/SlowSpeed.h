// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "interactable.h"
#include "SlowSpeed.generated.h"

UCLASS()
class SNAKEGAMEV24_API ASlowSpeed : public AActor, public Iinteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASlowSpeed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
